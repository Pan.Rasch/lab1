package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.printf("%s %d%n", "Let's play round", roundCounter);
            String compChoice = computerChoice();
            String urChoice = yourChoice();
            String Choices = "Human chose " + urChoice + ", computer chose " + compChoice + ".";
            if (winner(urChoice, compChoice)) {
                System.out.println(Choices + " Human wins!");
                humanScore++;
            }
            else if (winner(compChoice, urChoice)) {
                System.out.println(Choices + " Computer wins!");
                computerScore++;
            }
            else {
                System.out.println(Choices + " It's a tie!");
            }
            System.out.printf("Score: human %d, computer %d%n", humanScore, computerScore);
            String continueansw = continuePlaying();
            if (continueansw.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
        roundCounter++;
        }
    }

    public String yourChoice() {
        while(true) {
            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            if (rpsChoices.contains(userChoice)) {
                return userChoice;
            }
            else {
                System.out.println("I don't understand. Try again.");
            }
        }
    }

    public String computerChoice() {
        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return randomElement;
    }

    public boolean winner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }
    }

    public String continuePlaying() {
        while(true) {
            String continueans = readInput("Do you wish to continue playing? (y/n)?");
            if (continueans.equals("y") || continueans.equals("n")) {
            return continueans;
            }
            else {
                System.out.println("I don't understand. Try again.");
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput.toLowerCase();
    }

}
